using System;
using GigaVolt.Core.Abstractions.Services;
using Microsoft.AspNetCore.Mvc;

namespace GigaVolt.Gateway.Controllers
{
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly IOrderService _orderService;

        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpPost]
        public IActionResult CreateOrder()
        {
            throw new NotImplementedException();
        }
    }
}