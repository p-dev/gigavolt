using System.Linq;
using GigaVolt.Core.Abstractions.Services;
using GigaVolt.Gateway.Models.ViewModels.Responses;
using Microsoft.AspNetCore.Mvc;

namespace GigaVolt.Gateway.Controllers
{
    [ApiController]
    [Route("api/components")]
    public class ComponentsController : ControllerBase
    {
        private readonly IComponentService _componentService;

        public ComponentsController(IComponentService componentService)
        {
            _componentService = componentService;
        }

        [HttpGet("all")]
        public ActionResult<GetComponentsResponse> GetAll()
        {
            var components = _componentService.GetAll();
            
            var response = new GetComponentsResponse(components.GroupBy(x => x.GetType().Name).ToList());

            return response;
        }
    }
}