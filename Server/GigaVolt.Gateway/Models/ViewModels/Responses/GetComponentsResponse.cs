using System.Collections.Generic;
using System.Linq;
using GigaVolt.Core.Abstractions.Models.Components;

namespace GigaVolt.Gateway.Models.ViewModels.Responses
{
    public class GetComponentsResponse
    {
        public GetComponentsResponse(IReadOnlyCollection<IGrouping<string, IComponent>> components)
        {
            Components = components;
        }

        public IReadOnlyCollection<IGrouping<string, IComponent>> Components { get; }
    }
}