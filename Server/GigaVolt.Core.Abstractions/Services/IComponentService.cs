using System;
using System.Collections.Generic;
using System.Linq;
using GigaVolt.Core.Abstractions.Models.Components;

namespace GigaVolt.Core.Abstractions.Services
{
    public interface IComponentService
    {
        IReadOnlyCollection<IComponent> GetAll();
        
        IComponent GetById(Guid id);
    }

    public static class ComponentServiceExtensions
    {
        public static TComponent GetById<TComponent>(this IComponentService componentService, Guid id)
            where TComponent : IComponent
        {
            return (TComponent) componentService.GetById(id);
        }
    }
}