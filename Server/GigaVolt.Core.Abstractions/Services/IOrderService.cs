using GigaVolt.Core.Abstractions.Models;

namespace GigaVolt.Core.Abstractions.Services
{
    public interface IOrderService
    {
        void CreateOrder(Order order);
    }
}