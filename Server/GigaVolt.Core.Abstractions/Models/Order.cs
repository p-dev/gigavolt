using System;

namespace GigaVolt.Core.Abstractions.Models
{
    public class Order
    {
        public Order(DateTime time, Guid id, Board board)
        {
            Time = time;
            Id = id;
            Board = board;
        }

        public Guid Id { get; }
        
        public DateTime Time { get; }
        
        public Board Board { get; }
    }
}