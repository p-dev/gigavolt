using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Pulley : IComponent
    {
        public Guid Id { get; }
        public string Model { get; }
        
        public int TeethCount { get; }
        public float Width { get; }
    }
}