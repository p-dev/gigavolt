using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Enclosure : IComponent
    {
        public Enclosure(Guid id, string model)
        {
            Id = id;
            Model = model;
        }

        public Guid Id { get; }
        public string Model { get; }
        
//        public Size Size { get; }
    }
}