using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Deck : IComponent
    {
        public Deck(Guid id, string model, string material, float flexability)
        {
            Material = material;
            Flexability = flexability;
            Id = id;
            Model = model;
        }

        public Guid Id { get; }
        public string Model { get; }
        
        public string Material { get; }
        public float Flexability { get; }
        public float Size { get; }
        public string Form { get; }
    }
}