using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Controller : IComponent
    {
        public Controller(Guid id, string model)
        {
            Id = id;
            Model = model;
        }

        public Guid Id { get; }
        public string Model { get; }
        
        
    }
}