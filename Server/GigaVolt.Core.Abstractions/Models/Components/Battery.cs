using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Battery : IComponent
    {
        public Battery(Guid id, string model, float voltage, float amperage, float power)
        {
            Voltage = voltage;
            Amperage = amperage;
            Power = power;
            Id = id;
            Model = model;
        }

        public Guid Id { get; }
        public string Model { get; }
        
        public float Voltage { get; }
        public float Amperage { get; }
        public float Power { get; }
        public string Configuration { get; }
    }
}