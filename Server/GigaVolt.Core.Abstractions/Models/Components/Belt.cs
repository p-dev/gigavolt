using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Belt : IComponent
    {
        public Guid Id { get; }
        public string Model { get; }
    }
}