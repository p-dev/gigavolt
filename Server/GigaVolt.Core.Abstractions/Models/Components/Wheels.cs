using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Wheels : IComponent
    {
        public Wheels(Guid id, string model, float softness, float diameter)
        {
            Durometer = softness;
            Diameter = diameter;
            Model = model;
            Id = id;
        }

        public Guid Id { get; }
        public string Model { get; }
        
        public float Durometer { get; }
        public float Diameter { get; }
        public float Width { get; }
    }
}