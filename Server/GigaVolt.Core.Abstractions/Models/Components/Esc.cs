using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Esc : IComponent
    {
        public Guid Id { get; }
        public string Model { get; }
        
        public float MaxAmperage { get; }
        public EscKind Kind { get; }
    }

    public enum EscKind
    {
        Singular,
        Dual,
    }
}