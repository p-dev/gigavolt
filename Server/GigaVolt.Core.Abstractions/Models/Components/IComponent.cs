using System;
using System.Collections.Generic;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public interface IComponent
    {
        Guid Id { get; }
        string Model { get; }
        
//        IReadOnlyDictionary<string, string> Properties { get; }
    }
}