using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Bearing : IComponent
    {
        public Bearing(Guid id, string model)
        {
            Id = id;
            Model = model;
        }

        public Guid Id { get; }
        public string Model { get; }
        
        
    }
}