using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Trucks : IComponent
    {
        public Guid Id { get; }
        public string Model { get; }
    }
}