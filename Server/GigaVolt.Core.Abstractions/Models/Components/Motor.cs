using System;

namespace GigaVolt.Core.Abstractions.Models.Components
{
    public class Motor : IComponent
    {
        public Guid Id { get; }
        public string Model { get; }
        
        public float Power { get; }
        public float Width { get; }
        public float Diameter { get; }
    }
}