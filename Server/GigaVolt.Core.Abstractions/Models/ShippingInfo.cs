namespace GigaVolt.Core.Abstractions.Models
{
    public class ShippingInfo
    {
        public string FullName { get; }
        public string Email { get; }
        public string Telephone { get; }
        public string Address { get; }
    }
}