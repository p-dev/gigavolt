using System.Collections.Generic;
using System.Linq;
using GigaVolt.Core.Abstractions.Models.Components;

namespace GigaVolt.Core.Abstractions.Models
{
    public class Board
    {
        public Board(Deck deck, Controller controller, IReadOnlyCollection<IComponent> optionalComponents)
        {
            Deck = deck;
            Controller = controller;
            OptionalComponents = optionalComponents;
        }

        public Deck Deck { get; }
        public Controller Controller { get; }
        public Wheels Wheels { get; }
        public Enclosure Enclosure { get; }
        public Battery Battery { get; }
        public Bearing Bearing { get; }
        
        public IReadOnlyCollection<IComponent> OptionalComponents { get; }

        public IReadOnlyCollection<IComponent> Components
        {
            get
            {
                var components = new List<IComponent>()
                {
                    Deck, Controller,
                };
                components.AddRange(OptionalComponents);
                return components;
            }
        }
    }
}