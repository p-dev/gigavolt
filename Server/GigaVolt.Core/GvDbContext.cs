using GigaVolt.Core.Models.Daos;
using Microsoft.EntityFrameworkCore;

namespace GigaVolt.Core
{
    public sealed class GvDbContext : DbContext
    {
        public DbSet<OrderDao> Orders { get; }

        public GvDbContext(DbContextOptions options) : base(options)
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<OrderDao>(builder =>
            {
                
            });
        }
    }
}