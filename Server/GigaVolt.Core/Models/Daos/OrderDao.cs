using System;

namespace GigaVolt.Core.Models.Daos
{
    public class OrderDao
    {
        public OrderDao(Guid id)
        {
            Id = id;
        }

        public Guid Id { get; }
        
        public DateTime Time { get; }
    }
}