using System.Reflection;
using AutoMapper;
using GigaVolt.Core.Abstractions.Services;
using GigaVolt.Core.MappingProfiles;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;

namespace GigaVolt.Core.Services
{
    public static class ServiceExtensions
    {
        public class DomainToViewModelMappingProfile : Profile
        {
            
        }
        
        public static IServiceCollection AddOrderService(this IServiceCollection services)
        {
            services.AddTransient<IOrderService, OrderService>();
            
            services.AddAutoMapper(config =>
            {
                config.AddProfile<DaoToDomainMappingProfile>();
            }, Assembly.GetEntryAssembly());
            services.AddAutoMapper(config =>
            {
                config.AddProfile<DomainToViewModelMappingProfile>();
            }, Assembly.GetEntryAssembly());
            
            return services;
        }
    }
}