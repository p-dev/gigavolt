using AutoMapper;
using GigaVolt.Core.Abstractions.Models;
using GigaVolt.Core.Models.Daos;

namespace GigaVolt.Core.MappingProfiles
{
    public class DaoToDomainMappingProfile : Profile
    {
        public DaoToDomainMappingProfile()
        {
            CreateMap<OrderDao, Order>();
        }
    }
}